export interface BidimensionalSize {
    width: number,
    height: number
}
