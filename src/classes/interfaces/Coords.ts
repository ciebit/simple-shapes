export interface Coords {
    x?: number | string,
    y?: number | string
}
