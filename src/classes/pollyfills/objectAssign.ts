export function objectAssign() {
    if (! Object.assign) {
        Object.prototype.assign = (destiny, obj) => {
            for (let prop in obj) {
                if (!destiny.hasOwnProperty(prop)) {
                    destiny[prop] = obj[prop]
                }
            }
        }
    }
}

objectAssign();
