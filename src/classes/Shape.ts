import { Unity } from "./Unity"
import { Coords } from "./interfaces/Coords"
import { Command } from "./Commands/Command"
import { ReferencePoint } from "./Commands/ReferencePoint"
import { Line } from "./Commands/Line"
import { Curve } from "./Commands/Curve"
import { BidimensionalSize } from "./interfaces/BidimensionalSize"
import { CoordsWithUnities } from "./CoordsWithUnities";
import { convertStringToNumber } from "./util/convertStringToNumber";

export class Shape
{
    private border_color: string;
    private Unity: Unity
    private reference_point: ReferencePoint
    private path_commands: Array<Command>
    private svg: SVGElement
    private path: SVGPathElement
    private fill_color: string
    private canvas_size: BidimensionalSize

    public constructor()
    {
        this.path_commands = []
        this.referencePoint({ x: 0, y: 0 })
    }

    public borderColor(color: string): this
    {
        this.border_color = color
        return this
    }

    private calculateCurvature(coordinates, curve: number): number
    {
        let lastCoordinates = this.getLastCoordinate();
        console.group("Últimas coordenadas")
        console.log(lastCoordinates)
        console.groupEnd()

        let distance
        if (lastCoordinates.x - coordinates.x > lastCoordinates.y - coordinates.y) {
            distance = lastCoordinates.x - coordinates.x;
        } else {
            distance = lastCoordinates.y - coordinates.y;
        }

        return distance * curve / 100;
    }

    private calculateSimetricCurvature(coordinates: CoordsWithUnities, curve: number): {x:number, y:number}
    {
        let lastCoordinates = this.getLastCoordinate()

        let coords
        if (Math.abs(lastCoordinates.x) - Math.abs(coordinates.x) > Math.abs(lastCoordinates.y) - Math.abs(coordinates.y)) {
            let distanceX = coordinates.x - lastCoordinates.x
            let y = curve/100 * lastCoordinates.x
            let x = Math.abs(lastCoordinates.x) - Math.abs(distanceX)/2
            coords = {x: x, y: y}
        } else {
            let distanceY = coordinates.y - lastCoordinates.y
            let x = curve/100 * lastCoordinates.y
            let y = Math.abs(lastCoordinates.y) - Math.abs(distanceY)/2
            coords = {x: x, y: y}
        }
        return coords
    }

    public connectStartPoint(): this
    {
        this.path_commands.push(new Command);
        return this
    }

    private createPath(commands): string
    {
        console.group("Array de comandos")
        console.log(commands)
        console.groupEnd()

        let path = "";
        commands.forEach(command => {
            path += command.draw(this.Unity, this.canvas_size)
        })
        return path
    }

    public curve(coordinates: Coords, curve: number|string): this
    {
        let curveParsed = convertStringToNumber(curve)
        let coefficient = this.calculateCurvature(coordinates, curveParsed)

        let coordsStart = new CoordsWithUnities(coordinates, this.defineUnity(coefficient), this.defineUnity(coefficient))
        let coordsEnd = new CoordsWithUnities(coordinates, this.defineUnity(coordinates.x), this.defineUnity(coordinates.y))

        // let coordsStartRedimensioned = this.calculateDimension(coordsStart)
        // let coordsEndRedimensioned = this.calculateDimension(coordsEnd)

        this.path_commands.push(new Curve(coordsStart, coordsEnd))

        return this;
    }

    public draw(): this
    {
        let NS = "http://www.w3.org/2000/svg"
        let svgCanvas = <SVGElement> document.createElementNS(NS, "svg")
        let pathElement = <SVGPathElement> document.createElementNS(NS, "path")

        let pathString = this.createPath(this.path_commands)
        console.group("String comandos")
        console.log(pathString)
        console.groupEnd()

        svgCanvas.setAttributeNS(null, "width", this.canvas_size.width.toString())
        svgCanvas.setAttributeNS(null, "height", this.canvas_size.height.toString())
        pathElement.setAttributeNS(null, "d", pathString)
        pathElement.setAttributeNS(null, "fill", this.fill_color)
        pathElement.setAttributeNS(null, "stroke", this.border_color)

        // document.body.appendChild(svgCanvas);
        svgCanvas.appendChild(pathElement);
        this.path = pathElement;
        this.svg = svgCanvas;

        return this;
    }

    public fillColor(color: string): this
    {
        this.fill_color = color
        return this
    }

    private getLastCoordinate(): CoordsWithUnities
    {
        let lastCommand = this.path_commands[this.path_commands.length - 1];

        if (lastCommand instanceof Line) {
            return lastCommand.getCoords();
        }

        if (lastCommand instanceof Curve) {
            return lastCommand.getCoordsEnd();
        }
        return this.reference_point.getCoords();
    }

    public getSVG(): SVGElement
    {
        return this.svg
    }

    public getPath(): SVGPathElement
    {
        return this.path
    }

    public line(coordinates: Coords): this
    {
        let coords = coordinates
        let coordsWithUnities

        if (! coordinates.hasOwnProperty('y')) {
            coords.y = this.getLastCoordinate().y
            coordsWithUnities = new CoordsWithUnities(
                coords,
                this.defineUnity(coords.x),
                this.defineUnity(coords.y + this.getLastCoordinate().yUnity)
            )
        } else if (! coordinates.hasOwnProperty('x')) {
            coords.x = this.getLastCoordinate().x
            coordsWithUnities = new CoordsWithUnities(
                coords,
                this.defineUnity(coords.x + this.getLastCoordinate().xUnity),
                this.defineUnity(coords.y)
            )
        } else {
            coordsWithUnities = new CoordsWithUnities(
                coordinates,
                this.defineUnity(coordinates.x),
                this.defineUnity(coordinates.y)
            )
        }

        this.path_commands.push(new Line(coordsWithUnities))
        return this
    }

    private defineUnity(value: number|string): Unity
    {
        if (typeof value === "string") {
            let strWithNoSpaces = value.trim()

            if (new RegExp('[0-9\.]+%$').test(strWithNoSpaces)) {
                return Unity.Percentage
            } else if (new RegExp('[0-9\.]+px$').test(strWithNoSpaces)) {
                return Unity.Pixels
            }
        }
        return this.Unity;
    }

    public referencePoint(coordinates: Coords): this
    {
        this.reference_point = new ReferencePoint(new CoordsWithUnities(coordinates));
        this.path_commands.splice(0, 1, this.reference_point)
        return this
    }

    public setCanvasSize(width:number, height:number): this
    {
        this.canvas_size = {width:width, height:height}
        return this
    }

    public setUnityDefault(unity: Unity): this
    {
        this.Unity = unity
        return this
    }

    public simetricCurve(coordinates: Coords, curve: number|string): this
    {
        let coordsEnd = new CoordsWithUnities(coordinates, this.defineUnity(coordinates.x), this.defineUnity(coordinates.y))

        let curveParsed = convertStringToNumber(curve)
        let coords = this.calculateSimetricCurvature(coordsEnd, curveParsed)

        let coordsStart = new CoordsWithUnities(coords, this.defineUnity(coords.x), this.defineUnity(coords.y))

        // let coordsEndRedimensioned = this.calculateDimension(coordsEnd)

        this.path_commands.push(new Curve(coordsStart, coordsEnd))

        return this;
    }
}
