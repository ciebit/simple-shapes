import { Coords } from "./interfaces/Coords"
import { Unity } from "./Unity"

export class CoordsWithUnities
{
    public x: number
    public y: number
    public xUnity: Unity
    public yUnity: Unity

    public constructor(coords: Coords, unityX?: Unity, unityY?: Unity)
    {
        this.setCoords(coords)
        this.setUnityX(unityX)
        this.setUnityY(unityY)
    }

    public setCoords(coords: Coords): this
    {
        this.x = typeof coords.x == 'string' ? parseInt(coords.x) : coords.x
        this.y = typeof coords.y == 'string' ? parseInt(coords.y) : coords.y
        return this
    }

    public setUnityX(unity: Unity): this
    {
        this.xUnity = unity
        return this
    }

    public setUnityY(unity: Unity): this
    {
        this.yUnity = unity
        return this
    }

    public getCoords(): {x:number, y:number}
    {
        return {x: this.x, y: this.y}
    }

    public getUnityX(): Unity
    {
        return this.xUnity
    }

    public getUnityY(): Unity
    {
        return this.yUnity
    }
}
