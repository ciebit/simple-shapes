import {Command} from "./Command"
import { CoordsWithUnities } from "../CoordsWithUnities";
import {BidimensionalSize} from "../interfaces/BidimensionalSize"
import {Unity} from "../Unity"

export class ReferencePoint extends Command {
    private coords: CoordsWithUnities

    public constructor(coords: CoordsWithUnities) {
        super('M');
        this.coords = coords
    }

    public getCoords(): CoordsWithUnities
    {
        return this.coords
    }

    public setCoords(coords: CoordsWithUnities): this
    {
        this.coords = coords;
        return this;
    }

    public draw(unity:Unity, canvas_size:BidimensionalSize):string
    {
        let coordsRedimensioned = this.calculateDimension(this.getCoords(), unity, canvas_size)
        return `${this.getElement()}${coordsRedimensioned.x},${coordsRedimensioned.y} `
    }
}
