import { Command } from "./Command";
import { CoordsWithUnities } from "../CoordsWithUnities";
import {BidimensionalSize} from "../interfaces/BidimensionalSize"
import {Unity} from "../Unity"

export class Curve extends Command {
    public start: CoordsWithUnities
    public end: CoordsWithUnities

    public constructor(start: CoordsWithUnities, end: CoordsWithUnities) {
        super("Q")
        this.start = start
        this.end = end
    }

    public getCoordsEnd(): CoordsWithUnities
    {
        return this.end;
    }

    public getCoordsStart(): CoordsWithUnities
    {
        return this.start;
    }

    public setCoordsEnd(coords: CoordsWithUnities): this
    {
        this.end = coords;
        return this;
    }

    public setCoordsStart(coords: CoordsWithUnities): this
    {
        this.start = coords;
        return this;
    }

    public draw(unity:Unity, canvas_size:BidimensionalSize):string
    {
        let coordsStartRedimensioned = this.calculateDimension(this.getCoordsStart(), unity, canvas_size)
        let coordsEndRedimensioned = this.calculateDimension(this.getCoordsEnd(), unity, canvas_size)

        return `${this.getElement()}${coordsStartRedimensioned.x},${coordsStartRedimensioned.y} ${coordsEndRedimensioned.x},${coordsEndRedimensioned.y} `
    }
}
