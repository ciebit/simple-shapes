import { BidimensionalSize } from "../interfaces/BidimensionalSize"
import { CoordsWithUnities } from "../CoordsWithUnities"
import { Unity } from "../Unity"
import { objectAssign } from "../pollyfills/objectAssign"

export class Command {

    private element: string

    public constructor(element: string = "Z") {
        this.element = element;
    }

    public getElement(): string
    {
        return this.element;
    }

    public setElement(element: string): this
    {
        this.element = element;
        return this;
    }

    public draw(unity:Unity, canvas_size:BidimensionalSize):string
    {
        return `${this.getElement()}`
    }

    protected calculateDimension(coords: CoordsWithUnities, defaultUnity: Unity, canvas_size: BidimensionalSize): CoordsWithUnities
    {
        let coordsRedimensioned = Object.assign(new CoordsWithUnities(coords), coords)

        if (coords.xUnity == Unity.Percentage) {
            coordsRedimensioned.x = canvas_size.width * coords.x / 100
        } else if (coords.xUnity == Unity.Pixels ) {
            coordsRedimensioned.x = coords.x
        } else if (defaultUnity == Unity.Percentage) {
            coordsRedimensioned.x = canvas_size.width * coords.x / 100
        } else if (defaultUnity == Unity.Pixels){
            coordsRedimensioned.x = coords.x
        }

        if (coords.yUnity == Unity.Percentage) {
            coordsRedimensioned.y = canvas_size.height * coords.y / 100
        } else if (coords.yUnity == Unity.Pixels ) {
            coordsRedimensioned.y = coords.y
        } else if (defaultUnity == Unity.Percentage) {
            coordsRedimensioned.y = canvas_size.height * coords.y / 100
        } else if (defaultUnity == Unity.Pixels) {
            coordsRedimensioned.y = coords.y
        }

        return coordsRedimensioned
    }
}
