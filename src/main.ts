import { Shape } from "./classes/Shape"
import { Unity } from "./classes/Unity";

window.addEventListener("DOMContentLoaded", () => {
})
let shape = new Shape;

shape.setCanvasSize(500,500)
.setUnityDefault(Unity.Percentage)
.fillColor("blue")
.borderColor("red")

.line({x:"50"})
.line({y:"50px"})
.simetricCurve({x:0, y:"25%"}, 10)
// .line({x:0})
.connectStartPoint()
.draw()

document.body.appendChild(shape.getSVG())
