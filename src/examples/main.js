var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define("classes/Unity", ["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var Unity;
    (function (Unity) {
        Unity["Percentage"] = "%";
        Unity["Pixels"] = "px";
    })(Unity = exports.Unity || (exports.Unity = {}));
});
define("classes/interfaces/Coords", ["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
});
define("classes/interfaces/BidimensionalSize", ["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
});
define("classes/CoordsWithUnities", ["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var CoordsWithUnities = /** @class */ (function () {
        function CoordsWithUnities(coords, unityX, unityY) {
            this.setCoords(coords);
            this.setUnityX(unityX);
            this.setUnityY(unityY);
        }
        CoordsWithUnities.prototype.setCoords = function (coords) {
            this.x = typeof coords.x == 'string' ? parseInt(coords.x) : coords.x;
            this.y = typeof coords.y == 'string' ? parseInt(coords.y) : coords.y;
            return this;
        };
        CoordsWithUnities.prototype.setUnityX = function (unity) {
            this.xUnity = unity;
            return this;
        };
        CoordsWithUnities.prototype.setUnityY = function (unity) {
            this.yUnity = unity;
            return this;
        };
        CoordsWithUnities.prototype.getCoords = function () {
            return { x: this.x, y: this.y };
        };
        CoordsWithUnities.prototype.getUnityX = function () {
            return this.xUnity;
        };
        CoordsWithUnities.prototype.getUnityY = function () {
            return this.yUnity;
        };
        return CoordsWithUnities;
    }());
    exports.CoordsWithUnities = CoordsWithUnities;
});
define("classes/pollyfills/objectAssign", ["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    function objectAssign() {
        if (!Object.assign) {
            Object.prototype.assign = function (destiny, obj) {
                for (var prop in obj) {
                    if (!destiny.hasOwnProperty(prop)) {
                        destiny[prop] = obj[prop];
                    }
                }
            };
        }
    }
    exports.objectAssign = objectAssign;
    objectAssign();
});
define("classes/Commands/Command", ["require", "exports", "classes/CoordsWithUnities", "classes/Unity"], function (require, exports, CoordsWithUnities_1, Unity_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var Command = /** @class */ (function () {
        function Command(element) {
            if (element === void 0) { element = "Z"; }
            this.element = element;
        }
        Command.prototype.getElement = function () {
            return this.element;
        };
        Command.prototype.setElement = function (element) {
            this.element = element;
            return this;
        };
        Command.prototype.draw = function (unity, canvas_size) {
            return "" + this.getElement();
        };
        Command.prototype.calculateDimension = function (coords, defaultUnity, canvas_size) {
            var coordsRedimensioned = Object.assign(new CoordsWithUnities_1.CoordsWithUnities(coords), coords);
            if (coords.xUnity == Unity_1.Unity.Percentage) {
                coordsRedimensioned.x = canvas_size.width * coords.x / 100;
            }
            else if (coords.xUnity == Unity_1.Unity.Pixels) {
                coordsRedimensioned.x = coords.x;
            }
            else if (defaultUnity == Unity_1.Unity.Percentage) {
                coordsRedimensioned.x = canvas_size.width * coords.x / 100;
            }
            else if (defaultUnity == Unity_1.Unity.Pixels) {
                coordsRedimensioned.x = coords.x;
            }
            if (coords.yUnity == Unity_1.Unity.Percentage) {
                coordsRedimensioned.y = canvas_size.height * coords.y / 100;
            }
            else if (coords.yUnity == Unity_1.Unity.Pixels) {
                coordsRedimensioned.y = coords.y;
            }
            else if (defaultUnity == Unity_1.Unity.Percentage) {
                coordsRedimensioned.y = canvas_size.height * coords.y / 100;
            }
            else if (defaultUnity == Unity_1.Unity.Pixels) {
                coordsRedimensioned.y = coords.y;
            }
            return coordsRedimensioned;
        };
        return Command;
    }());
    exports.Command = Command;
});
define("classes/Commands/ReferencePoint", ["require", "exports", "classes/Commands/Command"], function (require, exports, Command_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var ReferencePoint = /** @class */ (function (_super) {
        __extends(ReferencePoint, _super);
        function ReferencePoint(coords) {
            var _this = _super.call(this, 'M') || this;
            _this.coords = coords;
            return _this;
        }
        ReferencePoint.prototype.getCoords = function () {
            return this.coords;
        };
        ReferencePoint.prototype.setCoords = function (coords) {
            this.coords = coords;
            return this;
        };
        ReferencePoint.prototype.draw = function (unity, canvas_size) {
            var coordsRedimensioned = this.calculateDimension(this.getCoords(), unity, canvas_size);
            return "" + this.getElement() + coordsRedimensioned.x + "," + coordsRedimensioned.y + " ";
        };
        return ReferencePoint;
    }(Command_1.Command));
    exports.ReferencePoint = ReferencePoint;
});
define("classes/Commands/Line", ["require", "exports", "classes/Commands/Command"], function (require, exports, Command_2) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var Line = /** @class */ (function (_super) {
        __extends(Line, _super);
        function Line(coords) {
            var _this = _super.call(this, 'L') || this;
            _this.coords = coords;
            return _this;
        }
        Line.prototype.getCoords = function () {
            return this.coords;
        };
        Line.prototype.setCoords = function (coords) {
            this.coords = coords;
            return this;
        };
        Line.prototype.draw = function (unity, canvas_size) {
            var coordsRedimensioned = this.calculateDimension(this.getCoords(), unity, canvas_size);
            return "" + this.getElement() + coordsRedimensioned.x + "," + coordsRedimensioned.y + " ";
        };
        return Line;
    }(Command_2.Command));
    exports.Line = Line;
});
define("classes/Commands/Curve", ["require", "exports", "classes/Commands/Command"], function (require, exports, Command_3) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var Curve = /** @class */ (function (_super) {
        __extends(Curve, _super);
        function Curve(start, end) {
            var _this = _super.call(this, "Q") || this;
            _this.start = start;
            _this.end = end;
            return _this;
        }
        Curve.prototype.getCoordsEnd = function () {
            return this.end;
        };
        Curve.prototype.getCoordsStart = function () {
            return this.start;
        };
        Curve.prototype.setCoordsEnd = function (coords) {
            this.end = coords;
            return this;
        };
        Curve.prototype.setCoordsStart = function (coords) {
            this.start = coords;
            return this;
        };
        Curve.prototype.draw = function (unity, canvas_size) {
            var coordsStartRedimensioned = this.calculateDimension(this.getCoordsStart(), unity, canvas_size);
            var coordsEndRedimensioned = this.calculateDimension(this.getCoordsEnd(), unity, canvas_size);
            return "" + this.getElement() + coordsStartRedimensioned.x + "," + coordsStartRedimensioned.y + " " + coordsEndRedimensioned.x + "," + coordsEndRedimensioned.y + " ";
        };
        return Curve;
    }(Command_3.Command));
    exports.Curve = Curve;
});
define("classes/util/convertStringToNumber", ["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    function convertStringToNumber(value) {
        if (typeof value === 'string') {
            return parseInt(value);
        }
        return value;
    }
    exports.convertStringToNumber = convertStringToNumber;
});
define("classes/Shape", ["require", "exports", "classes/Unity", "classes/Commands/Command", "classes/Commands/ReferencePoint", "classes/Commands/Line", "classes/Commands/Curve", "classes/CoordsWithUnities", "classes/util/convertStringToNumber"], function (require, exports, Unity_2, Command_4, ReferencePoint_1, Line_1, Curve_1, CoordsWithUnities_2, convertStringToNumber_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var Shape = /** @class */ (function () {
        function Shape() {
            this.path_commands = [];
            this.referencePoint({ x: 0, y: 0 });
        }
        Shape.prototype.borderColor = function (color) {
            this.border_color = color;
            return this;
        };
        Shape.prototype.calculateCurvature = function (coordinates, curve) {
            var lastCoordinates = this.getLastCoordinate();
            console.group("Últimas coordenadas");
            console.log(lastCoordinates);
            console.groupEnd();
            var distance;
            if (lastCoordinates.x - coordinates.x > lastCoordinates.y - coordinates.y) {
                distance = lastCoordinates.x - coordinates.x;
            }
            else {
                distance = lastCoordinates.y - coordinates.y;
            }
            return distance * curve / 100;
        };
        Shape.prototype.calculateSimetricCurvature = function (coordinates, curve) {
            var lastCoordinates = this.getLastCoordinate();
            var coords;
            if (Math.abs(lastCoordinates.x) - Math.abs(coordinates.x) > Math.abs(lastCoordinates.y) - Math.abs(coordinates.y)) {
                var distanceX = coordinates.x - lastCoordinates.x;
                var y = curve / 100 * lastCoordinates.x;
                var x = Math.abs(lastCoordinates.x) - Math.abs(distanceX) / 2;
                coords = { x: x, y: y };
            }
            else {
                var distanceY = coordinates.y - lastCoordinates.y;
                var x = curve / 100 * lastCoordinates.y;
                var y = Math.abs(lastCoordinates.y) - Math.abs(distanceY) / 2;
                coords = { x: x, y: y };
            }
            return coords;
        };
        Shape.prototype.connectStartPoint = function () {
            this.path_commands.push(new Command_4.Command);
            return this;
        };
        Shape.prototype.createPath = function (commands) {
            var _this = this;
            console.group("Array de comandos");
            console.log(commands);
            console.groupEnd();
            var path = "";
            commands.forEach(function (command) {
                path += command.draw(_this.Unity, _this.canvas_size);
            });
            return path;
        };
        Shape.prototype.curve = function (coordinates, curve) {
            var curveParsed = convertStringToNumber_1.convertStringToNumber(curve);
            var coefficient = this.calculateCurvature(coordinates, curveParsed);
            var coordsStart = new CoordsWithUnities_2.CoordsWithUnities(coordinates, this.defineUnity(coefficient), this.defineUnity(coefficient));
            var coordsEnd = new CoordsWithUnities_2.CoordsWithUnities(coordinates, this.defineUnity(coordinates.x), this.defineUnity(coordinates.y));
            // let coordsStartRedimensioned = this.calculateDimension(coordsStart)
            // let coordsEndRedimensioned = this.calculateDimension(coordsEnd)
            this.path_commands.push(new Curve_1.Curve(coordsStart, coordsEnd));
            return this;
        };
        Shape.prototype.draw = function () {
            var NS = "http://www.w3.org/2000/svg";
            var svgCanvas = document.createElementNS(NS, "svg");
            var pathElement = document.createElementNS(NS, "path");
            var pathString = this.createPath(this.path_commands);
            console.group("String comandos");
            console.log(pathString);
            console.groupEnd();
            svgCanvas.setAttributeNS(null, "width", this.canvas_size.width.toString());
            svgCanvas.setAttributeNS(null, "height", this.canvas_size.height.toString());
            pathElement.setAttributeNS(null, "d", pathString);
            pathElement.setAttributeNS(null, "fill", this.fill_color);
            pathElement.setAttributeNS(null, "stroke", this.border_color);
            // document.body.appendChild(svgCanvas);
            svgCanvas.appendChild(pathElement);
            this.path = pathElement;
            this.svg = svgCanvas;
            return this;
        };
        Shape.prototype.fillColor = function (color) {
            this.fill_color = color;
            return this;
        };
        Shape.prototype.getLastCoordinate = function () {
            var lastCommand = this.path_commands[this.path_commands.length - 1];
            if (lastCommand instanceof Line_1.Line) {
                return lastCommand.getCoords();
            }
            if (lastCommand instanceof Curve_1.Curve) {
                return lastCommand.getCoordsEnd();
            }
            return this.reference_point.getCoords();
        };
        Shape.prototype.getSVG = function () {
            return this.svg;
        };
        Shape.prototype.getPath = function () {
            return this.path;
        };
        Shape.prototype.line = function (coordinates) {
            var coords = coordinates;
            var coordsWithUnities;
            if (!coordinates.hasOwnProperty('y')) {
                coords.y = this.getLastCoordinate().y;
                coordsWithUnities = new CoordsWithUnities_2.CoordsWithUnities(coords, this.defineUnity(coords.x), this.defineUnity(coords.y + this.getLastCoordinate().yUnity));
            }
            else if (!coordinates.hasOwnProperty('x')) {
                coords.x = this.getLastCoordinate().x;
                coordsWithUnities = new CoordsWithUnities_2.CoordsWithUnities(coords, this.defineUnity(coords.x + this.getLastCoordinate().xUnity), this.defineUnity(coords.y));
            }
            else {
                coordsWithUnities = new CoordsWithUnities_2.CoordsWithUnities(coordinates, this.defineUnity(coordinates.x), this.defineUnity(coordinates.y));
            }
            this.path_commands.push(new Line_1.Line(coordsWithUnities));
            return this;
        };
        Shape.prototype.defineUnity = function (value) {
            if (typeof value === "string") {
                var strWithNoSpaces = value.trim();
                if (new RegExp('[0-9\.]+%$').test(strWithNoSpaces)) {
                    return Unity_2.Unity.Percentage;
                }
                else if (new RegExp('[0-9\.]+px$').test(strWithNoSpaces)) {
                    return Unity_2.Unity.Pixels;
                }
            }
            return this.Unity;
        };
        Shape.prototype.referencePoint = function (coordinates) {
            this.reference_point = new ReferencePoint_1.ReferencePoint(new CoordsWithUnities_2.CoordsWithUnities(coordinates));
            this.path_commands.splice(0, 1, this.reference_point);
            return this;
        };
        Shape.prototype.setCanvasSize = function (width, height) {
            this.canvas_size = { width: width, height: height };
            return this;
        };
        Shape.prototype.setUnityDefault = function (unity) {
            this.Unity = unity;
            return this;
        };
        Shape.prototype.simetricCurve = function (coordinates, curve) {
            var coordsEnd = new CoordsWithUnities_2.CoordsWithUnities(coordinates, this.defineUnity(coordinates.x), this.defineUnity(coordinates.y));
            var curveParsed = convertStringToNumber_1.convertStringToNumber(curve);
            var coords = this.calculateSimetricCurvature(coordsEnd, curveParsed);
            var coordsStart = new CoordsWithUnities_2.CoordsWithUnities(coords, this.defineUnity(coords.x), this.defineUnity(coords.y));
            // let coordsEndRedimensioned = this.calculateDimension(coordsEnd)
            this.path_commands.push(new Curve_1.Curve(coordsStart, coordsEnd));
            return this;
        };
        return Shape;
    }());
    exports.Shape = Shape;
});
define("main", ["require", "exports", "classes/Shape", "classes/Unity"], function (require, exports, Shape_1, Unity_3) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    window.addEventListener("DOMContentLoaded", function () {
    });
    var shape = new Shape_1.Shape;
    shape.setCanvasSize(500, 500)
        .setUnityDefault(Unity_3.Unity.Percentage)
        .fillColor("blue")
        .borderColor("red")
        .line({ x: "50" })
        .line({ y: "50px" })
        .simetricCurve({ x: 0, y: "25%" }, 10)
        .connectStartPoint()
        .draw();
    document.body.appendChild(shape.getSVG());
});
